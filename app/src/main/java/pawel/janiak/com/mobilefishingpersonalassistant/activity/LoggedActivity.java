package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.User;
import pawel.janiak.com.mobilefishingpersonalassistant.database.dbUser;
import pawel.janiak.com.mobilefishingpersonalassistant.helper.SQLiteHandler;
import pawel.janiak.com.mobilefishingpersonalassistant.helper.SessionManager;


public class LoggedActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    private Button btnLogout;
    private TextView tvLogin, tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged);

        final SessionManager sessionManager = new SessionManager(getApplicationContext());

        btnLogout = (Button) findViewById(R.id.btnLogout);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        tvEmail = (TextView) findViewById(R.id.tvEmail);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());

        final dbUser mDb = new dbUser(getApplicationContext());
        getUserDetails(mDb);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessionManager.isLoggedIn()) {
                    sessionManager.setLogin(false);
                    mDb.deleteUsers();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }

    public void getUserDetails(dbUser mDb){
        for (User u:mDb.getUser()){
            tvLogin.setText("Imię: "+ u.getName());
            tvEmail.setText("Email: " + u.getEmail());
            Log.d("wszystkie",u.getName()+" "+u.getEmail() );
        }

    }

}
