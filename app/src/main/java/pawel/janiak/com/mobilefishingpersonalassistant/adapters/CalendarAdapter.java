package pawel.janiak.com.mobilefishingpersonalassistant.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import java.util.List;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyDay;


public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder>{
    private Context mContext;
    private List<MyDay> myDays;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView data, wschod, zachod, faza, branie;
        public ImageView thumbnail;


        public MyViewHolder(final View view) {
            super(view);

            data = (TextView) view.findViewById(R.id.day_data);
            wschod = (TextView) view.findViewById(R.id.day_wschod);
            zachod = (TextView) view.findViewById(R.id.day_zachod);
            faza = (TextView) view.findViewById(R.id.day_faza);
            branie = (TextView) view.findViewById(R.id.day_branie);
            thumbnail = (ImageView) view.findViewById(R.id.ivMoon);
        }
    }

    public CalendarAdapter(Context mContext, List<MyDay> myDays) {
        this.mContext = mContext;
        this.myDays = myDays;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_calendar_template, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        MyDay myDay = myDays.get(position);
        holder.data.setText("Data: "+myDay.getData());
        holder.wschod.setText("Wschod: "+myDay.getWschod());
        holder.zachod.setText("Zachod: "+myDay.getZachod());
        holder.faza.setText("Faza: "+myDay.getFaza());
        holder.branie.setText("Branie: "+myDay.getBranie());

        // loading album cover using Glide library
        Glide.with(mContext).load(myDay.getThumbnail()).into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return myDays.size();
    }
}
