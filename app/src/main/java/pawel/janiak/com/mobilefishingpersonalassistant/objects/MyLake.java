package pawel.janiak.com.mobilefishingpersonalassistant.objects;


public class MyLake {

    private Long id;
    private String nazwa;
    private String opis;
    private String ryby;
    private String porady;
    private double latitude;
    private double longitude;
    private float alpha;

    public MyLake(String nazwa, String opis, String ryby, String porady, double latitude, double longitude, float alpha){
        this.nazwa = nazwa;
        this.opis = opis;
        this.ryby = ryby;
        this.porady = porady;
        this.latitude = latitude;
        this.longitude = longitude;
        this.alpha = alpha;
    }


    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getNazwa(){
        return nazwa;
    }
    public void setNazwa(String nazwa){
        this.nazwa = nazwa;
    }
    public String getOpis() {
        return opis;
    }
    public void setOpis(String opis){
        this.opis = opis;
    }
    public String getRyby(){
        return ryby;
    }
    public void setRyby(String ryby){
        this.ryby = ryby;
    }
    public String getPorady() {
        return porady;
    }
    public void setPorady(String porady) {
        this.porady = porady;
    }
    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public float getAlpha(){
        return alpha;
    }
    public void setAlpha(float alpha){
        this.alpha = alpha;
    }
    
}
