package pawel.janiak.com.mobilefishingpersonalassistant.app;


public class AppConfig {

    // Server user login url
    public static String URL_LOGIN = "http://mfpa.cba.pl/android_login_api/login.php";

    // Server user register url
    public static String URL_REGISTER = "http://mfpa.cba.pl/android_login_api/register.php";

    // server user share post url
    public static String URL_SHAREPOST = "http://mfpa.cba.pl/android_login_api/share_posts.php";

    // server user get id
    public static String URL_GETPOSTS = "http://mfpa.cba.pl/android_login_api/getPost.php";
}
