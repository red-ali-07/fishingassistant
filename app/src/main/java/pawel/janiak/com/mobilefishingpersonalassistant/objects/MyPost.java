package pawel.janiak.com.mobilefishingpersonalassistant.objects;


public class MyPost {
    private String jezioro;
    private String opis;
    private String autor;

    public MyPost(String jezioro, String opis, String autor){
        this.jezioro = jezioro;
        this.opis = opis;
        this.autor = autor;
    }

    public String getJezioro(){
        return jezioro;
    }
    public void setJezioro(String jezioro){
        this.jezioro = jezioro;
    }
    public String getOpis(){
        return opis;
    }
    public void setOpis(String opis){
        this.opis = opis;
    }
    public String getAutor(){
        return autor;
    }
    public void setAutor(String autor){
        this.autor = autor;
    }
}
