package pawel.janiak.com.mobilefishingpersonalassistant.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyDay;

public class DbMoon extends SQLiteOpenHelper {

    public DbMoon(Context context) {
        super(context, "kalendarz", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table kalendarzksiezycowy(id integer primary key autoincrement, data text, wschod text, zachod text, faza text, thumbnail integer, branie text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    public void addMyDay(MyDay myDay){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues wartosci = new ContentValues();
        wartosci.put("data", myDay.getData());
        wartosci.put("wschod",myDay.getWschod());
        wartosci.put("zachod",myDay.getZachod());
        wartosci.put("faza",myDay.getFaza());
        wartosci.put("thumbnail",myDay.getThumbnail());
        wartosci.put("branie",myDay.getBranie());
        db.insertOrThrow("kalendarzksiezycowy",null, wartosci);
    }

    public void deleteMyDay(int id){
        SQLiteDatabase db = getWritableDatabase();
        String[] argumenty={""+id};
        db.delete("kalendarzksiezycowy", "id=?", argumenty);
    }


    public void updateMyDay(MyDay myDay){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues wartosci = new ContentValues();
        wartosci.put("data", myDay.getData());
        wartosci.put("wschod",myDay.getWschod());
        wartosci.put("zachod",myDay.getZachod());
        wartosci.put("faza",myDay.getFaza());
        wartosci.put("thumbnail",myDay.getThumbnail());
        wartosci.put("branie",myDay.getBranie());
        String args[]={myDay.getId()+""};
        db.update("kalendarzksiezycowy", wartosci,"id=?",args);
    }

    public List<MyDay> getAll(){
        List<MyDay> myDays = new LinkedList<MyDay>();
        String[] kolumny={"id","data","wschod","zachod","faza","thumbnail","branie"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor kursor =db.query("kalendarzksiezycowy",kolumny,null,null,null,null,null);
        while(kursor.moveToNext()){
            MyDay myDay = new MyDay(null,null,null,null,0,null);
            myDay.setId(kursor.getLong(0));
            myDay.setData(kursor.getString(1));
            myDay.setWschod(kursor.getString(2));
            myDay.setZachod(kursor.getString(3));
            myDay.setFaza(kursor.getString(4));
            myDay.setThumbnail(kursor.getInt(5));
            myDay.setBranie(kursor.getString(6));
            myDays.add(myDay);
        }
        return myDays;
    }

    /*
    public myDay getmyDays(int nr){
        myDay myDay = new myDay(null,null,null,null,null);
        SQLiteDatabase db = getReadableDatabase();
        String[] kolumny={"id","data","wschod","zachod","faza","branie"};
        String args[]={nr+""};
        Cursor kursor=db.query("kalendarzksiezycowy",kolumny," id=?",args,null,null,null,null);
        if(kursor!=null){
            kursor.moveToFirst();
            myDay.setId(kursor.getLong(0));
            myDay.setData(kursor.getString(1));
            myDay.setWschod(kursor.getString(2));
            myDay.setZachod(kursor.getString(3));
            myDay.setFaza(kursor.getString(4));
            myDay.setBranie(kursor.getString(5));
        }
        kursor.close();
        return myDay;
    }

    public myDay getmyDay(String data){
        myDay myDay = new myDay(null,null,null,null,null);
        SQLiteDatabase db = getReadableDatabase();
        String[] kolumny={"id","data","wschod","zachod","faza","branie"};
        String args[]={data+""};
        Cursor kursor=db.query("kalendarzksiezycowy",kolumny," data=?",args,null,null,null,null);
        if(kursor!=null){
            kursor.moveToFirst();
            myDay.setId(kursor.getLong(0));
            myDay.setData(kursor.getString(1));
            myDay.setWschod(kursor.getString(2));
            myDay.setZachod(kursor.getString(3));
            myDay.setFaza(kursor.getString(4));
            myDay.setBranie(kursor.getString(5));
        }
        kursor.close();
        return myDay;
    }
    */
}
