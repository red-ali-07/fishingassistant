package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.adapters.CalendarAdapter;
import pawel.janiak.com.mobilefishingpersonalassistant.database.DbLake;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyDay;
import pawel.janiak.com.mobilefishingpersonalassistant.database.DbMoon;
import pawel.janiak.com.mobilefishingpersonalassistant.listeners.RecyclerItemClickListener;
import pawel.janiak.com.mobilefishingpersonalassistant.receiver.NotificationReceiver;

public class CalendarActivity extends AppCompatActivity {


    private LinearLayout mLlCalendar, mLlNodes, mLlMaps, mLlNotepad, mLlAtlas;
    private CalendarActivity ins;
    private int time = 1000;
    private static final String PREFS_NAME = "pref";
    private static Intent mIntent;
    private RecyclerView recyclerView;
    private CalendarAdapter adapter;
    private List<MyDay> myDays;
    private AlarmManager alarmManager;
    private boolean mboolean = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_main);

        ins = this;
        bottomNavigationBar();

        final Context mContext = getApplicationContext();

        // navigation drawer
        Toolbar toolbar = (Toolbar) findViewById(R.id.calendar_toolbar);
        setSupportActionBar(toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_person_white_24dp);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DbMoon mDb = new DbMoon(getApplicationContext());

        myDays = new ArrayList<>();
        adapter = new CalendarAdapter(this, myDays);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_calendar);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareCalendar(mDb);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        int i = position;

                    }
                })
        );


        /*
        final SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        checkBox.setChecked(settings.getBoolean("auto", false));
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("auto", checkBox.isChecked());
                editor.commit();

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY,10);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                if (isChecked){
                    Intent intent = new Intent(getApplicationContext(),NotificationReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),100,intent,PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,pendingIntent);
                    Toast.makeText(getApplicationContext(),"Notification set at: " + calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND),Toast.LENGTH_LONG).show();                }
                else {
                    Intent intent = new Intent(getApplicationContext(), NotificationReceiver.class);
                    PendingIntent sender = PendingIntent.getBroadcast(getApplicationContext(), 100, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

                    alarmManager.cancel(sender);
                    Toast.makeText(getApplicationContext(),"Notification off",Toast.LENGTH_LONG).show();
                }
            }
        });
        */
    }

    private void bottomNavigationBar(){


        // animation
        final ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this);

        // handler
        final Handler handler = new Handler();

        // tie
        mLlCalendar = (LinearLayout) findViewById(R.id.llCalendar);
        mLlNodes = (LinearLayout) findViewById(R.id.llNodes);
        mLlMaps = (LinearLayout) findViewById(R.id.llMaps);
        mLlNotepad = (LinearLayout) findViewById(R.id.llNotepad);
        mLlAtlas = (LinearLayout) findViewById(R.id.llAtlas);

        // LISTENERS
        // calendar
        mLlCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(ins, CalendarActivity.class);
                //ActivityCompat.startActivity(ins, intent, options.toBundle());
                //finish();
            }
        });
        // nodes
        mLlNodes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, NodesActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // maps
        mLlMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, MainActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // notepad
        mLlNotepad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, NotepadActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // atlas
        mLlAtlas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, AtlasActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent main = new Intent(this, LoggedActivity.class);
                startActivity(main);
                return true;

            case R.id.logout:
                Toast.makeText(getApplicationContext(), "title", Toast.LENGTH_LONG).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void prepareCalendar(DbMoon mDb) {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String formatedDate = df.format(calendar.getTime());
        int count = 0;

        // wyciaganie 7 kolejnych dni, liczac od dnia obecnego
        for (MyDay k:mDb.getAll()){
            if (formatedDate.equals(k.getData())){
                Long i = k.getId();
                for (MyDay m:mDb.getAll()){
                    if (m.getId().equals(i+count) && count<7) {
                        MyDay d = new MyDay(m.getData(),m.getWschod(),m.getZachod(),m.getFaza(),m.getThumbnail(),m.getBranie());
                        myDays.add(d);
                        count+=1;
                    }
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

}
