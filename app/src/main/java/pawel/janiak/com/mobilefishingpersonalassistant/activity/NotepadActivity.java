package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.database.DbNotepad;

public class NotepadActivity extends AppCompatActivity {

    private LinearLayout mLlCalendar, mLlNodes, mLlMaps, mLlNotepad, mLlAtlas;
    private NotepadActivity ins;
    private int time = 1000;
    DbNotepad mydb;
    com.github.clans.fab.FloatingActionButton btnAdd;
    ListView mylist;
    CoordinatorLayout coordinatorLayout;
    SimpleCursorAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notepad);

        ins = this;
        bottomNavigationBar();

        // navigation drawer
        Toolbar Toolbar = (Toolbar) findViewById(R.id.notepad_toolbar);
        setSupportActionBar(Toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_person_white_24dp);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // layout
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        mydb = new DbNotepad(this);
        btnAdd = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_btn_add);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", 0);
                Intent intent = new Intent(getApplicationContext(), SingleNoteActivity.class);
                intent.putExtras(dataBundle);
                startActivity(intent);
                finish();
            }
        });

        Cursor c = mydb.fetchAll();
        String[] fieldNames = new String[] { DbNotepad.name, DbNotepad._id, DbNotepad.dates, DbNotepad.remark };

        int[] display = new int[] { R.id.txtnamerow, R.id.txtidrow, R.id.txtdate,R.id.txtremark };



        adapter = new SimpleCursorAdapter(this, R.layout.single_note_template2, c, fieldNames, display, 0);


        mylist = (ListView) findViewById(R.id.listView1);
        mylist.setAdapter(adapter);

        mylist.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                CardView linearLayoutParent = (CardView) arg1;
                LinearLayout linearLayoutChild = (LinearLayout) linearLayoutParent.getChildAt(0);
                TextView m = (TextView) linearLayoutChild.getChildAt(1);

                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", Integer.parseInt(m.getText().toString()));

                Intent intent = new Intent(getApplicationContext(), SingleNoteActivity.class);

                intent.putExtras(dataBundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void bottomNavigationBar(){


        // animation
        final ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this);

        // handler
        final Handler handler = new Handler();

        // tie
        mLlCalendar = (LinearLayout) findViewById(R.id.llCalendar);
        mLlNodes = (LinearLayout) findViewById(R.id.llNodes);
        mLlMaps = (LinearLayout) findViewById(R.id.llMaps);
        mLlNotepad = (LinearLayout) findViewById(R.id.llNotepad);
        mLlAtlas = (LinearLayout) findViewById(R.id.llAtlas);

        // LISTENERS
        // calendar
        mLlCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, CalendarActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // nodes
        mLlNodes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, NodesActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // maps
        mLlMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, MainActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // notepad
        mLlNotepad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(ins, NotepadActivity.class);
                //ActivityCompat.startActivity(ins, intent, options.toBundle());
                //finish();
            }
        });
        // atlas
        mLlAtlas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, AtlasActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent main = new Intent(this, LoggedActivity.class);
                startActivity(main);
                return true;

            case R.id.logout:
                Toast.makeText(getApplicationContext(), "title", Toast.LENGTH_LONG).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //mIntent = new Intent(getApplicationContext(),MainActivity.class);
        //startActivity(mIntent);
        finish();
    }
}