package pawel.janiak.com.mobilefishingpersonalassistant.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyPost;


public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder>{
    private Context mContext;
    private List<MyPost> myPosts;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView jezioro, opis, autor;


        public MyViewHolder(final View view) {
            super(view);

            jezioro = (TextView) view.findViewById(R.id.post_jezioro);
            opis = (TextView) view.findViewById(R.id.post_opis);
            autor = (TextView) view.findViewById(R.id.post_autor);
        }
    }

    public PostAdapter(Context mContext, List<MyPost> myPosts) {
        this.mContext = mContext;
        this.myPosts = myPosts;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_post_template, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        MyPost myPost = myPosts.get(position);
        holder.jezioro.setText("Łowisko: "+myPost.getJezioro());
        holder.opis.setText("Opis: "+myPost.getOpis());
        holder.autor.setText("Autor: "+myPost.getAutor());

    }

    @Override
    public int getItemCount() {
        return myPosts.size();
    }
}
