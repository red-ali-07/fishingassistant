package pawel.janiak.com.mobilefishingpersonalassistant.activity;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyDay;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyLake;
import pawel.janiak.com.mobilefishingpersonalassistant.database.DbLake;
import pawel.janiak.com.mobilefishingpersonalassistant.database.DbMoon;
import pawel.janiak.com.mobilefishingpersonalassistant.helper.SessionManager;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener, GoogleMap.OnMarkerClickListener {

    private LinearLayout mLlCalendar, mLlNodes, mLlMaps, mLlNotepad, mLlAtlas;
    private MainActivity ins;
    private int time = 1000;
    private Criteria criteria;
    private LocationManager locationManager;
    private LatLng coordinate;
    private GoogleMap mMap;
    private boolean GPSStatus = true;
    private Intent mIntent;
    private DbLake mDbLake;
    private DbMoon mDbCalendar;
    private SessionManager session;
    private com.google.android.gms.maps.model.Marker myPosition, przystanek;
    public final static String myCurrentPosition = "My Current Position";
    public final static String targetPosition = "Target Position";
    public final static String NAZWA_ID = "nazwa";
    public final static String OPIS_ID = "opis";
    public final static String RYBY_ID = "ryby";
    public final static String PORADY_ID = "porady";
    public final static String LAT_ID = "lat";
    public final static String LONG_ID = "long";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);

        ins = this;
        bottomNavigationBar();

        final SessionManager sessionManager = new SessionManager(getApplicationContext());

        // wywolanie metody jednorazowej
        mDbCalendar = new DbMoon(getApplicationContext());
        mDbLake = new DbLake(getApplicationContext());
        onesCalledMethod(mDbCalendar, mDbLake);


        // save my position
        criteria = new Criteria();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final String provider = locationManager.getBestProvider(criteria, false);

        final com.github.clans.fab.FloatingActionMenu mMenu;
        mMenu = (com.github.clans.fab.FloatingActionMenu) findViewById(R.id.menu);
        final com.github.clans.fab.FloatingActionButton mSaveMyPosition;
        mSaveMyPosition = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.save_my_position);
        mSaveMyPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // zamyka FAB menu
                mMenu.close(true);

                if (ActivityCompat.checkSelfPermission(ins, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ins, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                if (locationManager.getLastKnownLocation(provider) != null) {
                    Location location = locationManager.getLastKnownLocation(provider);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.single_alert_dialog, null);
                    builder.setView(dialogView);
                    final EditText etName = (EditText) dialogView.findViewById(R.id.myPositionName);
                    final EditText etSnippet = (EditText) dialogView.findViewById(R.id.myPositionSnippet);

                    builder.setPositiveButton("Zapisz", null)
                            .setNegativeButton("anuluj", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    // utowrzenie okna dialogowego
                    final AlertDialog alert = builder.create();

                    // utworzenie listenera dla okna dialogowego
                    alert.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialog) {
                            Button btnPositive = alert.getButton(alert.BUTTON_POSITIVE);
                            btnPositive.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // jesli tytul jest pusty poinformuj użytkownika i nie zamykaj okna dialogowego
                                    if (etName.length() == 0) {
                                        Toast.makeText(getApplicationContext(), "Wprowadź nazwę", Toast.LENGTH_LONG).show();
                                    } else {
                                        if (ActivityCompat.checkSelfPermission(ins, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ins, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                            // TODO: Consider calling
                                            //    ActivityCompat#requestPermissions
                                            // here to request the missing permissions, and then overriding
                                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                            //                                          int[] grantResults)
                                            // to handle the case where the user grants the permission. See the documentation
                                            // for ActivityCompat#requestPermissions for more details.
                                            return;
                                        }
                                        Location location = locationManager.getLastKnownLocation(provider);
                                        double lat = location.getLatitude();
                                        double lng = location.getLongitude();
                                        coordinate = new LatLng(lat, lng);

                                        // dodanie markera do mapy
                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(lat, lng))
                                                .anchor(0.5f, 0.5f)
                                                .alpha(0.98f)
                                                .title(etName.getText().toString())
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_place)));

                                        // dodanie markera do bazy
                                        MyLake l = new MyLake(
                                                String.valueOf(etName.getText()),
                                                String.valueOf(etSnippet.getText()),
                                                null,
                                                null,
                                                lat,
                                                lng,
                                                0.98f);
                                        mDbLake.addMyLake(l);

                                        // zamkniecie okna dialogowego
                                        alert.dismiss();
                                    }
                                }
                            });
                        }
                    });
                    alert.show();
                } else
                    Toast.makeText(v.getContext(), "oczekiwanie na sygnał GPS", Toast.LENGTH_SHORT).show();

            }
        });


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        // maps
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "GPS włączony", Toast.LENGTH_SHORT).show();
        } else {
            showGPSDisabledAlertToUser();
        }


        // navigation drawer
        Toolbar Toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        Toolbar.setTitle("Mapa");
        setSupportActionBar(Toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_person_white_24dp);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // przechodzimy ekranu do logowania jesli nie jestesmy zalogowani
        if (!sessionManager.isLoggedIn()) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent main = new Intent(this, LoggedActivity.class);
                startActivity(main);
                return true;

            case R.id.logout:
                Toast.makeText(getApplicationContext(), "title", Toast.LENGTH_LONG).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // metoda sprawdzajaca wlaczenie gpsu i pozwalajaca przejsc do ustawien
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS jest wyłączony. Chcesz włączyć?")
                .setCancelable(false)
                .setPositiveButton("Przejdź do ustawień",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("anuluj",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    // metoda zarządzająca mapą
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);

        android.location.LocationListener locationListener = new android.location.LocationListener() {
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onLocationChanged(Location location) {
                if (GPSStatus) {
                    refresh();
                    GPSStatus = false;
                }
            }
        };

        criteria = new Criteria();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(provider, 500, 0, locationListener);

        // Add a marker in somewhere and move the camera
        final LatLng somewhere = new LatLng(53.4166667, 14.5833333);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(somewhere, 12));

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 1));
                return false;
            }
        });

        // dodawanie markerów do mapy
        for (MyLake lake : mDbLake.getAll()) {
            if (lake.getAlpha() == 1.0f) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lake.getLatitude(), lake.getLongitude()))
                        .anchor(0.5f, 0.5f)
                        .alpha(1.0f)
                        .title(lake.getNazwa())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.lake)));
            } else if (lake.getAlpha() == 0.99f) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lake.getLatitude(), lake.getLongitude()))
                        .anchor(0.5f, 0.5f)
                        .alpha(0.99f)
                        .title(lake.getNazwa())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.fav)));
            } else if (lake.getAlpha() == 0.98f) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lake.getLatitude(), lake.getLongitude()))
                        .anchor(0.5f, 0.5f)
                        .alpha(0.98f)
                        .title(lake.getNazwa())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_place)));
            }
        }

        // onMapClickListener do dodawania markerow w miejscu klikniecia
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(final LatLng latLng) {
                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.myDialog));
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.single_alert_dialog, null);
                builder.setView(dialogView);

                final EditText etName = (EditText) dialogView.findViewById(R.id.myPositionName);
                final EditText etSnippet = (EditText) dialogView.findViewById(R.id.myPositionSnippet);
                builder.setCancelable(false)
                        .setPositiveButton("Zapisz", null)
                        .setNeutralButton("anuluj", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                final AlertDialog alert = builder.create();

                // utworzenie listenera dla okna dialogowego
                alert.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Button btnPositive = alert.getButton(alert.BUTTON_POSITIVE);
                        btnPositive.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // jesli tytul jest pusty poinformuj użytkownika i nie zamykaj okna dialogowego
                                if (etName.length() == 0) {
                                    Toast.makeText(getApplicationContext(), "Wprowadź nazwę", Toast.LENGTH_LONG).show();
                                } else {
                                    // dodanie markera do mapy
                                    mMap.addMarker(new MarkerOptions()
                                            .title(etName.getText().toString())
                                            .snippet(etSnippet.getText().toString())
                                            .position(new LatLng(latLng.latitude, latLng.longitude))
                                            .anchor(0.5f, 0.5f)
                                            .alpha(0.99f)
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.fav)));

                                    // dodanie markera do bazy
                                    MyLake l = new MyLake(String.valueOf(etName.getText()), String.valueOf(etSnippet.getText()), null, null, latLng.latitude, latLng.longitude, 0.99f);
                                    mDbLake.addMyLake(l);

                                    // zamkniecie okna dialogowego
                                    alert.dismiss();
                                }
                            }
                        });
                    }
                });
                alert.show();
            }
        });

    }


    // metoda zarządzająca wybieraniem elementów z navDrawera
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Pobranie identyfikatora elementu z NavigationDrawer.
        int id = item.getItemId();

        // Zarządzanie akcją dla konkretnego elementu.
        switch (id) {
            case R.id.logIn:
                mIntent = new Intent(getApplicationContext(), LoggedActivity.class);
                startActivity(mIntent);
                break;
            case R.id.NotepadId:
                mIntent = new Intent(getApplicationContext(), NotepadActivity.class);
                startActivity(mIntent);
                break;
            case R.id.KnotsId:
                mIntent = new Intent(getApplicationContext(), NodesActivity.class);
                startActivity(mIntent);
                break;
            case R.id.CalendarId:
                mIntent = new Intent(getApplicationContext(), CalendarActivity.class);
                startActivity(mIntent);
                break;
            case R.id.SharedPostsId:
                mIntent = new Intent(getApplicationContext(), PostsActivity.class);
                startActivity(mIntent);
                break;
            default:
                break;
        }

        // Utworzenie obiektu DrawerLayout potrzebnego do zarządzania
        // NavigationDrawer, i uzyskanie odniesienia do pola w Layout.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            // Zamknięcie NavigationDrawer po wybraniu elementu.
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }


    // odświeżanie aktualnej pozycji GPS
    public void refresh() {

        criteria = new Criteria();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(criteria, false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (locationManager.getLastKnownLocation(provider) != null) {
            Toast.makeText(this, "Znaleziono sygnał GPS", Toast.LENGTH_SHORT).show();
            Location location = locationManager.getLastKnownLocation(provider);
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            coordinate = new LatLng(lat, lng);
            Log.e("Coordinates ", coordinate.toString());
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().isZoomControlsEnabled();
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 14));
        } else {
            Toast.makeText(this, "brak sygnału GPS", Toast.LENGTH_SHORT).show();
            mMap.setMyLocationEnabled(false);
        }
    }


    // odpowiada za klknięcie przycisku wstecz
    @Override
    public void onBackPressed() {
        finish();
    }


    // odpowiada za klikniecie w marker
    @Override
    public boolean onMarkerClick(final com.google.android.gms.maps.model.Marker marker) {

        // wylaczenie przyciskow nawigowania dla markera
        mMap.getUiSettings().setMapToolbarEnabled(false);

        // onLongClick marker
        if (marker.getAlpha() == 0.99f) {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(marker
                    .getSnippet())
                    .setTitle(marker.getTitle());


            alertDialogBuilder.setPositiveButton("Pokaż kierunek", null);
            alertDialogBuilder.setNegativeButton("Usuń marker", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mDbLake.deleteMyLake(marker.getTitle());
                    marker.remove();
                }
            });
            alertDialogBuilder.setNeutralButton("anuluj",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            final AlertDialog alert = alertDialogBuilder.create();

            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button btnPositive = alert.getButton(alert.BUTTON_POSITIVE);
                    btnPositive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                            String provider = locationManager.getBestProvider(criteria, false);
                            if (ActivityCompat.checkSelfPermission(ins, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ins, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            Location location = locationManager.getLastKnownLocation(provider);

                            if (locationManager.getLastKnownLocation(provider) == null){
                                Toast.makeText(getApplicationContext(),"Oczekiwanie na sygnał GPS",Toast.LENGTH_LONG).show();
                            } else {
                                LatLng mTargetPosition = marker.getPosition();
                                double lat = location.getLatitude();
                                double lng = location.getLongitude();
                                coordinate = new LatLng(lat, lng);
                                mIntent = new Intent(getApplicationContext(), CompassActivity.class);
                                mIntent.putExtra(myCurrentPosition, coordinate);
                                mIntent.putExtra(targetPosition, mTargetPosition);

                                startActivity(mIntent);

                                // zamkniecie okna dialogowego
                                alert.dismiss();
                            }
                        }
                    });
                }
            });

            alert.show();
        }

        // saveMyPosition marker
        else if (marker.getAlpha() == 0.98f){
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(marker.getSnippet())
                    .setTitle(marker.getTitle());
            alertDialogBuilder.setPositiveButton("pokaz wiecej informacji",
                    new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id){
                            Intent mIntent = new Intent(getApplicationContext(),LakeActivity.class);
                            mIntent.putExtra(NAZWA_ID, marker.getTitle());
                            mIntent.putExtra(OPIS_ID,marker.getSnippet());
                            startActivity(mIntent);
                        }
                    });
            alertDialogBuilder.setNegativeButton("Usuń marker",
                    new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id) {
                            mDbLake.deleteMyLake(marker.getTitle());
                            marker.remove();
                        }
                    });
            alertDialogBuilder.setNeutralButton("anuluj",
                    new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id){
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }

        // lake marker
        if (marker.getAlpha() == 1.0f){
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(marker.getSnippet())
                    .setTitle(marker.getTitle());
            alertDialogBuilder.setPositiveButton("pokaz wiecej informacji",
                    new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id){
                            Intent mIntent = new Intent(getApplicationContext(),LakeActivity.class);
                            for (MyLake lake:mDbLake.getAll()) {
                                if (lake.getNazwa().equals(marker.getTitle())){
                                    mIntent.putExtra(NAZWA_ID,lake.getNazwa());
                                    mIntent.putExtra(OPIS_ID,lake.getOpis());
                                    mIntent.putExtra(RYBY_ID,lake.getRyby());
                                    mIntent.putExtra(PORADY_ID,lake.getPorady());
                                    mIntent.putExtra(LAT_ID, lake.getLatitude());
                                    mIntent.putExtra(LONG_ID,lake.getLongitude());
                                }
                            }
                            startActivity(mIntent);
                        }
                    });
            alertDialogBuilder.setNegativeButton("anuluj",
                    new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id){
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }



        return false;
    }


    private void bottomNavigationBar(){


        // animation
        final ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this);

        // handler
        final Handler handler = new Handler();

        // tie
        mLlCalendar = (LinearLayout) findViewById(R.id.llCalendar);
        mLlNodes = (LinearLayout) findViewById(R.id.llNodes);
        mLlMaps = (LinearLayout) findViewById(R.id.llMaps);
        mLlNotepad = (LinearLayout) findViewById(R.id.llNotepad);
        mLlAtlas = (LinearLayout) findViewById(R.id.llAtlas);

        // LISTENERS
        // calendar
        mLlCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, CalendarActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // nodes
        mLlNodes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, NodesActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // maps
        mLlMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Intent intent = new Intent(ins, MainActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
                */
            }
        });
        // notepad
        mLlNotepad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, NotepadActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // atlas
        mLlAtlas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, AtlasActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
    }


    // wywołana jednorazowo po pierwszym uruchomieniu
    public void onesCalledMethod(DbMoon mDbCalendar, DbLake mDbLake){

        boolean mboolean = false;

        SharedPreferences settings = getSharedPreferences("PREFS_NAME", 0);
        mboolean = settings.getBoolean("FIRST_RUN", false);
        if (!mboolean) {

            // wywolanie metody dodawania rekordow kalendarza oraz dodawania rekordkow do jezior
            addToCalendar(mDbCalendar);
            addToLakeDatabase(mDbLake);

            settings = getSharedPreferences("PREFS_NAME", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("FIRST_RUN", true);
            editor.commit();

        } else {Log.e("Wywołanie","wywolano kolejny raz");}
    }


    // metoda dodawania danych do bazy kalendarza
    public void addToCalendar(DbMoon mDb){

        /* szablon
            d = new MyMyMyDay("","","","","");
            mDb.addMyMyDay(d);

            */

        int[] covers = new int[]{
                R.drawable.moon1pelnia,         // pelnia
                R.drawable.moon2garbaty,        // garbaty księżyc
                R.drawable.moon3trzeciakwadra,  // trzecia kwadra
                R.drawable.moon4malejacy,       // malejący sierp
                R.drawable.moon5now,            // nów
                R.drawable.moon6wzrastajacy,    // wzrastający sierp
                R.drawable.moon7pierwszakwarda, // pierwsza kwadra
                R.drawable.moon8poszerzony};       // poszerzony

        // grudzien
        MyDay d = new MyDay("16.12.2016","7:40","15:23","garbaty księżyc", covers[1],"złe");
        mDb.addMyDay(d);
        d = new MyDay("17.12.2016","7:40","15:23","garbaty księżyc", covers[1],"złe");
        mDb.addMyDay(d);
        d = new MyDay("18.12.2016","7:41","15:24","garbaty księżyc", covers[1],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("19.12.2016","7:42","15:24","trzecia kwadra", covers[2],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("20.12.2016","7:42","15:24","trzecia kwadra", covers[2],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("21.12.2016","7:43","15:25","trzecia kwadra", covers[2],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("22.12.2016","7:43","15:25","trzecia kwadra", covers[2],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("23.12.2016","7:44","15:26","malejący sierp", covers[3],"złe");
        mDb.addMyDay(d);
        d = new MyDay("24.12.2016","7:44","15:27","malejący sierp", covers[3],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("25.12.2016","7:44","15:27","malejący sierp", covers[3],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("26.12.2016","7:45","15:28","malejący sierp", covers[3],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("27.12.2016","7:45","15:29","nów", covers[4],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("28.12.2016","7:45","15:30","nów", covers[4],"bardzo dobre");
        mDb.addMyDay(d);
        d = new MyDay("29.12.2016","7:45","15:31","nów", covers[4],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("30.12.2016","7:45","15:32","wzrastający sierp", covers[5],"złe");
        mDb.addMyDay(d);
        d = new MyDay("31.12.2016","7:45","15:33","wzrastający sierp", covers[5],"złe");

        // styczen 2017
        mDb.addMyDay(d);
        d = new MyDay("01.01.2017","7:45","15:34","nów", covers[4],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("02.01.2017","7:45","15:35","wzrastający sierp", covers[5],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("03.01.2017","7:44","15:36","wzrastający sierp", covers[5],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("04.01.2017","7:44","15:37","wzrastający sierp", covers[5],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("05.01.2017","7:44","15:38","pierwsza kwadra", covers[6],"słabe");
        mDb.addMyDay(d);
        d = new MyDay("06.01.2017","7:43","15:40","pierwsza kwadra", covers[6],"słabe");
        mDb.addMyDay(d);
        d = new MyDay("07.01.2017","7:43","15:41","pierwsza kwadra", covers[6],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("08.01.2017","7:42","15:42","pierwsza kwadra", covers[6],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("09.01.2017","7:42","15:44","poszerzony księżyc", covers[7],"słabe");
        mDb.addMyDay(d);
        d = new MyDay("10.01.2017","7:41","15:45","poszerzony księżyc", covers[7],"słabe");
        mDb.addMyDay(d);
        d = new MyDay("11.01.2017","7:40","15:47","poszerzony księżyc", covers[7],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("12.01.2017","7:40","15:48","poszerzony księżyc", covers[7],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("13.01.2017","7:39","15:50","pełnia",covers[0],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("14.01.2017","7:38","15:51","pełnia",covers[0],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("15.01.2017","7:37","15:53","pełnia",covers[0],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("16.01.2017","7:36","15:54","pełnia",covers[0],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("17.01.2017","7:35","15:56","garbaty księżyc", covers[1],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("18.01.2017","7:34","15:58","garbaty księżyc", covers[1],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("19.01.2017","7:33","15:59","garbaty księżyc", covers[1],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("20.01.2017","7:32","16:01","trzecia kwadra", covers[2],"bardzo dobre");
        mDb.addMyDay(d);
        d = new MyDay("21.01.2017","7:31","16:03","trzecia kwadra", covers[2],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("22.01.2017","7:30","16:04","trzecia kwadra", covers[2],"słabe");
        mDb.addMyDay(d);
        d = new MyDay("23.01.2017","7:28","16:06","trzecia kwadra", covers[2],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("24.01.2017","7:27","16:08","malejący sierp", covers[3],"średnie");
        mDb.addMyDay(d);
        d = new MyDay("25.01.2017","7:26","16:10","malejący sierp", covers[3],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("26.01.2017","7:24","16:12","malejący sierp", covers[3],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("27.01.2017","7:23","16:13","malejący sierp", covers[3],"bardzo dobre");
        mDb.addMyDay(d);
        d = new MyDay("28.01.2017","7:22","16:15","nów", covers[4],"dobre");
        mDb.addMyDay(d);
        d = new MyDay("29.01.2017","7:20","16:17","nów", covers[4],"słabe");
        mDb.addMyDay(d);
        d = new MyDay("30.01.2017","7:19","16:19","nów", covers[4],"słabe");
        mDb.addMyDay(d);
        d = new MyDay("31.01.2017","7:17","16:21","nów", covers[4],"średnie");
        mDb.addMyDay(d);


    }


    // metoda dodawania danych do bazy jezior
    public void addToLakeDatabase(DbLake mDb){

        float alphaLake = 1.0f;
        MyLake l = new MyLake(
                "Jezioro Orle Małe",
                "Długość jeziora wynosi około 1000 m , szerokość 300 ( max. 400) metrów." +
                        "Głębokość ponoć 25 m , jest sporo głębokich rowów, woda bardzo czysta , jezioro otoczone trzcinami , lecz jest sporo fajnych miejscówek.\n" +
                        "Dogodny dojazd z miejscowości Łowicz Wałecki oraz z Mirosławca ( trzeba jechać na miejscowość Orle i przed skrętem do Orla skręcić w\n" +
                        "lewo w leśną drogę drogowskaz do punktu czerpania wody z Mirosławca do drogi leśnej jest około 5,5 km . dogodny dojazd do jeziora ).",
                "ryby",
                "użyj kukurydzy",
                53.214, 16.041,
                alphaLake);
        mDb.addMyLake(l);

        l = new MyLake(
                "Kanał Zielony",
                "Kanał Zielony znajduje się między ulicą Celną a Przekopem Parnickim równolegle do torów(mostu kolejowego), " +
                        "kilkanaście(kilkadziesiąt) metrów obok jest salon Renault. Jest tam sporo miejsc do wędkowania więc każdy znajdzie coś dla siebie. " +
                        "Głębokość do ok. 6metrów.",
                "Na spławik płoć, leszcz, okoń. \nNa spining szczupak oraz okoń",
                "kukurydza, ciasto",
                53.418877, 14.560350,
                alphaLake);
        mDb.addMyLake(l);
    }

}
