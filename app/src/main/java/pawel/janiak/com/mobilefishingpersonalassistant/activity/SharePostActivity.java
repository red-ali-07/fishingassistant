package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.app.AppConfig;
import pawel.janiak.com.mobilefishingpersonalassistant.app.AppController;
import pawel.janiak.com.mobilefishingpersonalassistant.database.dbUser;
import pawel.janiak.com.mobilefishingpersonalassistant.helper.SQLiteHandler;
import pawel.janiak.com.mobilefishingpersonalassistant.helper.SessionManager;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.User;

public class SharePostActivity extends AppCompatActivity {

    private static final String TAG = SharePostActivity.class.getSimpleName();
    private EditText etSharePostLake, etSharePostDescription;
    private com.github.clans.fab.FloatingActionButton mBtnSharePost;
    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private String userEmail, userName;
    private int user_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_post);

        // toolbar
        Toolbar Toolbar = (Toolbar) findViewById(R.id.tb_sharePost);
        setSupportActionBar(Toolbar);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // pobieranie danych urzystkownika z lokalnej bazy
        dbUser mDb = new dbUser(getApplicationContext());

        for (User u:mDb.getUser()){
            userEmail = u.getEmail();
            user_id = u.getUserId();
            userName = u.getName();
        }

        etSharePostLake = (EditText) findViewById(R.id.et_sharePost_lake);
        etSharePostDescription = (EditText) findViewById(R.id.et_sharePost_description);
        mBtnSharePost = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_btn_sahrePost);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Register Button Click event
        mBtnSharePost.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String lake = etSharePostLake.getText().toString().trim();
                String description = etSharePostDescription.getText().toString().trim();
                String author = userName.toString().trim();
                String usr_id = String.valueOf(user_id);

                if (!lake.isEmpty() && !description.isEmpty()) {
                    addPost(usr_id, lake, description, author);

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Wypełnij oba pola!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }


    /**
     * adding post
     * @param user_id
     * @param lake
     * @param description
     */
    private void addPost(final String user_id, final String lake, final String description, final String author) {
        // Tag used to cancel the request
        String tag_string_req = "req_sharePost";

        pDialog.setMessage("Dodawanie wpisu, proszę czekać ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SHAREPOST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Share MyPost Response: " + response.toString());
                hideDialog();

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        Toast.makeText(getApplicationContext(), "Dodano pomyślnie.", Toast.LENGTH_LONG).show();
                        finish();

                    } else {
                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Share post Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",user_id);
                params.put("lake", lake);
                params.put("description", description);
                params.put("author", author);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
