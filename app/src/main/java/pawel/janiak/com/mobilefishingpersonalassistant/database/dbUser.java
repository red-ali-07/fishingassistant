package pawel.janiak.com.mobilefishingpersonalassistant.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import pawel.janiak.com.mobilefishingpersonalassistant.objects.User;


public class dbUser extends SQLiteOpenHelper {
    public dbUser(Context context) {
        super(context, "User", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table user(" +
                        "id integer primary key autoincrement," +
                        "name text," +
                        "email text,"+
                        "user_id integer);" +
                        "");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {}

    public void addUser(User user){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name",user.getName());
        values.put("email",user.getEmail());
        values.put("user_id",user.getUserId());
        db.insertOrThrow("user",null, values);

    }

    public void deleteUsers(){
        SQLiteDatabase db = getWritableDatabase();
        db.delete("user", null, null);

    }

    public List<User> getUser() {
        List<User> users = new LinkedList<User>();
        String[] cols = {"id", "name", "email","user_id"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("user", cols, null, null, null, null, null);
        while (cursor.moveToNext()) {
            User user = new User(0, null, null,0);
            user.setId(cursor.getInt(0));
            user.setName(cursor.getString(1));
            user.setEmail(cursor.getString(2));
            user.setUser_id(cursor.getInt(3));
            users.add(user);
        }
        return users;
    }

}
