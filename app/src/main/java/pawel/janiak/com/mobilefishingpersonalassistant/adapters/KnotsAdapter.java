package pawel.janiak.com.mobilefishingpersonalassistant.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.List;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyKnot;


public class KnotsAdapter extends RecyclerView.Adapter<KnotsAdapter.MyViewHolder>{
    private Context mContext;
    private List<MyKnot> myKnotList;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;


        public MyViewHolder(final View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        }
    }

    public KnotsAdapter(Context mContext, List<MyKnot> myKnotList) {
        this.mContext = mContext;
        this.myKnotList = myKnotList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_knot_template, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        MyKnot myKnot = myKnotList.get(position);
        holder.title.setText(myKnot.getName());

        // loading album cover using Glide library
        Glide.with(mContext).load(myKnot.getThumbnail()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return myKnotList.size();
    }
}
