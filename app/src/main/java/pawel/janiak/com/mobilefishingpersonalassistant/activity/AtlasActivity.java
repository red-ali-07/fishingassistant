package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import pawel.janiak.com.mobilefishingpersonalassistant.R;

public class AtlasActivity extends AppCompatActivity {

    private LinearLayout mLlCalendar, mLlNodes, mLlMaps, mLlNotepad, mLlAtlas;
    private AtlasActivity ins;
    private int time = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atlas);

        ins = this;

        createToolbar();
        bottomNavigationBar();
    }

    private void createToolbar(){
        // navigation drawer
        Toolbar Toolbar = (Toolbar) findViewById(R.id.toolbar);
        Toolbar.setTitle(getString(R.string.atlas_activity));
        setSupportActionBar(Toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_person_white_24dp);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void bottomNavigationBar(){


        // animation
        final ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this);

        // handler
        final Handler handler = new Handler();

        // tie
        mLlCalendar = (LinearLayout) findViewById(R.id.llCalendar);
        mLlNodes = (LinearLayout) findViewById(R.id.llNodes);
        mLlMaps = (LinearLayout) findViewById(R.id.llMaps);
        mLlNotepad = (LinearLayout) findViewById(R.id.llNotepad);
        mLlAtlas = (LinearLayout) findViewById(R.id.llAtlas);

        // LISTENERS
        // calendar
        mLlCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, CalendarActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // nodes
        mLlNodes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, NodesActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // maps
        mLlMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, MainActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // notepad
        mLlNotepad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, NotepadActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // atlas
        mLlAtlas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(ins, AtlasActivity.class);
                //ActivityCompat.startActivity(ins, intent, options.toBundle());
                //finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent main = new Intent(this, LoggedActivity.class);
                startActivity(main);
                return true;

            case R.id.logout:
                Toast.makeText(getApplicationContext(), "title", Toast.LENGTH_LONG).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
