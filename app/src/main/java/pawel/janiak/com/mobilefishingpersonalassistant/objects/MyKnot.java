package pawel.janiak.com.mobilefishingpersonalassistant.objects;

public class MyKnot {
    private String name;
    private int thumbnail;
    private int count;

    public MyKnot(String name, int thumbnail, int count) {
        this.name = name;
        this.thumbnail = thumbnail;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
