package pawel.janiak.com.mobilefishingpersonalassistant.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.activity.CalendarActivity;
import pawel.janiak.com.mobilefishingpersonalassistant.activity.MainActivity;
import pawel.janiak.com.mobilefishingpersonalassistant.database.DbMoon;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyDay;

public class NotificationReceiver extends BroadcastReceiver {

    private String branie, wschod, zachod;
    private long faza;

    @Override
    public void onReceive(Context context, Intent intent) {

        DbMoon mDb = new DbMoon(context);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String formatedDate = df.format(calendar.getTime());

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Uri defaultSoundUri1= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent repeting_intent = new Intent(context,CalendarActivity.class);
        repeting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,100,repeting_intent,PendingIntent.FLAG_UPDATE_CURRENT);

        for (MyDay k:mDb.getAll()){
            if (formatedDate.equals(k.getData())){
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                        .setContentIntent(pendingIntent)
                        .setSound(defaultSoundUri1)
                        .setSmallIcon(k.getThumbnail(),10)
                        .setContentTitle("Branie: " + k.getBranie())
                        .setContentText("Wschód: "+k.getWschod()+"\nZachód: "+k.getZachod())
                        .setAutoCancel(true);

                notificationManager.notify(100, builder.build());
            }
        }




    }
}
