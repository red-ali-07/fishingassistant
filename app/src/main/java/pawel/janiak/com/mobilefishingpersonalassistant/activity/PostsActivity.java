package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import java.util.ArrayList;
import java.util.List;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.adapters.CalendarAdapter;
import pawel.janiak.com.mobilefishingpersonalassistant.adapters.PostAdapter;
import pawel.janiak.com.mobilefishingpersonalassistant.app.AppConfig;
import pawel.janiak.com.mobilefishingpersonalassistant.app.AppController;
import pawel.janiak.com.mobilefishingpersonalassistant.database.dbUser;
import pawel.janiak.com.mobilefishingpersonalassistant.helper.SQLiteHandler;
import pawel.janiak.com.mobilefishingpersonalassistant.listeners.RecyclerItemClickListener;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyPost;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.User;

public class PostsActivity extends AppCompatActivity {

    private static final String TAG = "Erory";
    private RecyclerView recyclerView;
    private ProgressDialog pDialog;
    private MyPost myPost;
    private PostAdapter adapter;
    private List<MyPost> myPosts;
    private SQLiteHandler db;
    private String userName;
    private com.github.clans.fab.FloatingActionButton mBtnAddPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);

        // navigation drawer
        Toolbar toolbar = (Toolbar) findViewById(R.id.calendar_toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    protected void onResume() {
        super.onResume();

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // pobieranie danych urzystkownika z lokalnej bazy
        dbUser mDb = new dbUser(getApplicationContext());
        for (User u:mDb.getUser()){
            userName = u.getName();
        }

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Proszę czekać, trwa pobieranie postów...");
        pDialog.setCancelable(false);

        final Context mContext = getApplicationContext();
        myPosts = new ArrayList<>();
        getPosts();
        adapter = new PostAdapter(this, myPosts);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_post);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        mBtnAddPost = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_btn_addPost);
        mBtnAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),SharePostActivity.class);
                startActivity(intent);
            }
        });


    }

    private void getPosts(){
        showpDialog();

        JsonArrayRequest req = new JsonArrayRequest(AppConfig.URL_GETPOSTS,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        try {
                            // Parsing json array response
                            // loop through each json object
                            for (int i = 0; i < response.length(); i++) {

                                JSONObject post = (JSONObject) response.get(i);

                                String jezioro = post.getString("lake");
                                String opis = post.getString("description");
                                String autor = post.getString("author");

                                myPost = new MyPost(jezioro,opis,autor);
                                myPosts.add(myPost);

                            }
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }

                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error.getMessage().equals("org.json.JSONException: Value <!-- of type java.lang.String cannot be converted to JSONArray")){
                    Toast.makeText(getApplicationContext(),  "Brak postów", Toast.LENGTH_SHORT).show();
                }
                hidepDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);

    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
