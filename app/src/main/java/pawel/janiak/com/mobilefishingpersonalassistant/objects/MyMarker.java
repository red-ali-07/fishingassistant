package pawel.janiak.com.mobilefishingpersonalassistant.objects;


public class MyMarker {

    private double latitude,longitude;
    private String title, snippet;

    public MyMarker(double latitude, double longitude, String title, String snippet){
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.snippet = snippet;
    }

    public double getLatitude(){
        return latitude;
    }
    public double getLongitude(){
        return longitude;
    }
    public String getTitle(){
        return title;
    }
    public String getSnippet(){
        return snippet;
    }

}
