package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.database.DbNotepad;

public class SingleNoteActivity extends AppCompatActivity {
    private DbNotepad mydb;
    EditText name;
    EditText content;
    private CoordinatorLayout coordinatorLayout;
    String dateString;
    int id_To_Update = 0;
    Snackbar snackbar;
    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_note);

        // navigation drawer
        Toolbar Toolbar = (Toolbar) findViewById(R.id.notepad_toolbar);
        setSupportActionBar(Toolbar);

        //Floating buttons
        com.github.clans.fab.FloatingActionButton mBtnSave, mBtnDelete;
        mBtnSave = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_btn_save);
        mBtnDelete = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_btn_delete);

        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                Calendar c = Calendar.getInstance();
                System.out.println("Current time => " + c.getTime());
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String formattedDate = df.format(c.getTime());
                dateString = formattedDate;
                if (extras != null) {
                    int Value = extras.getInt("id");
                    if (Value > 0) {
                        if (content.getText().toString().trim().equals("") || name.getText().toString().trim().equals("")) {
                            snackbar = Snackbar.make(coordinatorLayout, "Musisz wprowadzić tytuł notatki", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        } else {
                            if (mydb.updateNotes(id_To_Update, name.getText().toString(), dateString, content.getText().toString())) {
                                //snackbar = Snackbar.make(coordinatorLayout, "Zaktualizowano notatkę", Snackbar.LENGTH_LONG);
                                //snackbar.show();
                                Toast.makeText(SingleNoteActivity.this, "Zaktualizowano notatkę",Toast.LENGTH_SHORT).show();
                                mIntent = new Intent(getApplicationContext(),NotepadActivity.class);
                                startActivity(mIntent);
                                finish();
                            } else {
                                snackbar = Snackbar.make(coordinatorLayout, "Błąd", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        }
                    } else {
                        if (content.getText().toString().trim().equals("")
                                || name.getText().toString().trim().equals("")) {
                            snackbar = Snackbar
                                    .make(coordinatorLayout, "Wprowadź tekst notatki", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        } else {
                            if (mydb.insertNotes(name.getText().toString(), dateString, content.getText().toString())) {
                                //snackbar = Snackbar.make(coordinatorLayout, "Dodano", Snackbar.LENGTH_LONG);
                                //snackbar.show();
                                Toast.makeText(SingleNoteActivity.this, "Dodano",Toast.LENGTH_SHORT).show();
                                mIntent = new Intent(getApplicationContext(),NotepadActivity.class);
                                startActivity(mIntent);
                                finish();
                            } else {
                                snackbar = Snackbar.make(coordinatorLayout, "Błąd", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        }
                    }
                }
            }
        });

        mBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SingleNoteActivity.this);
                builder.setMessage(R.string.DeleteNote)
                        .setPositiveButton("TAK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        mydb.deleteNotes(id_To_Update);
                                        Toast.makeText(SingleNoteActivity.this, "Usunięto",Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), NotepadActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                        .setNegativeButton("NIE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                });
                AlertDialog d = builder.create();
                d.setTitle("Usuwanie notatki");
                d.show();;
            }
        });


        name = (EditText) findViewById(R.id.txtname);
        content = (EditText) findViewById(R.id.txtcontent);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        mydb = new DbNotepad(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int Value = extras.getInt("id");
            if (Value > 0) {
                snackbar = Snackbar.make(coordinatorLayout, "Id notatki : "+String.valueOf(Value), Snackbar.LENGTH_LONG);
                snackbar.show();
                Cursor rs = mydb.getData(Value);
                id_To_Update = Value;
                rs.moveToFirst();
                String nam = rs.getString(rs.getColumnIndex(DbNotepad.name));
                String contents = rs.getString(rs.getColumnIndex(DbNotepad.remark));
                if (!rs.isClosed()) {
                    rs.close();
                }
                name.setText((CharSequence) nam);
                content.setText((CharSequence) contents);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(
                getApplicationContext(),
                NotepadActivity.class);
        startActivity(intent);
        finish();
        return;
    }
}
