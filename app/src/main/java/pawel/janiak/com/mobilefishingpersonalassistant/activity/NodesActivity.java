package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pawel.janiak.com.mobilefishingpersonalassistant.R;
import pawel.janiak.com.mobilefishingpersonalassistant.adapters.KnotsAdapter;
import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyKnot;
import pawel.janiak.com.mobilefishingpersonalassistant.knots.KnotWezelBarylkowy;
import pawel.janiak.com.mobilefishingpersonalassistant.knots.KnotWezelHaczykowy;
import pawel.janiak.com.mobilefishingpersonalassistant.knots.KnotWezelOwijkowy;
import pawel.janiak.com.mobilefishingpersonalassistant.knots.KnotWezelPalomar;
import pawel.janiak.com.mobilefishingpersonalassistant.knots.KnotZakladanieZylkiNaSzpule;
import pawel.janiak.com.mobilefishingpersonalassistant.listeners.RecyclerItemClickListener;

public class NodesActivity extends AppCompatActivity {

    private LinearLayout mLlCalendar, mLlNodes, mLlMaps, mLlNotepad, mLlAtlas;
    private NodesActivity ins;
    private int time = 1000;
    private static Intent mIntent;
    private RecyclerView recyclerView;
    private KnotsAdapter adapter;
    private List<MyKnot> myKnotList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nodes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.knots_toolbar);
        setSupportActionBar(toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_person_white_24dp);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ins = this;
        bottomNavigationBar();

        final Context mContext = getApplicationContext();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_knots);

        myKnotList = new ArrayList<>();
        adapter = new KnotsAdapter(this, myKnotList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        int i = position;
                        switch (i){
                            case 3:
                                mIntent = new Intent(mContext,KnotWezelOwijkowy.class);
                                startActivity(mIntent);
                                break;
                            case 4:
                                mIntent = new Intent(mContext,KnotZakladanieZylkiNaSzpule.class);
                                startActivity(mIntent);
                                break;
                            case 0:
                                mIntent = new Intent(mContext,KnotWezelHaczykowy.class);
                                startActivity(mIntent);
                                break;
                            case 1:
                                mIntent = new Intent(mContext,KnotWezelPalomar.class);
                                startActivity(mIntent);
                                break;
                            case 2:
                                mIntent = new Intent(mContext,KnotWezelBarylkowy.class);
                                startActivity(mIntent);
                                break;
                            default:
                                break;
                        }
                    }
                })
        );
        prepareKnots();

    }

    private void bottomNavigationBar(){


        // animation
        final ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this);


        // handler
        final Handler handler = new Handler();

        // tie
        mLlCalendar = (LinearLayout) findViewById(R.id.llCalendar);
        mLlNodes = (LinearLayout) findViewById(R.id.llNodes);
        mLlMaps = (LinearLayout) findViewById(R.id.llMaps);
        mLlNotepad = (LinearLayout) findViewById(R.id.llNotepad);
        mLlAtlas = (LinearLayout) findViewById(R.id.llAtlas);

        // LISTENERS
        // calendar
        mLlCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, CalendarActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // nodes
        mLlNodes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(ins, NodesActivity.class);
                //ActivityCompat.startActivity(ins, intent, options.toBundle());
                //finish();
            }
        });
        // maps
        mLlMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, MainActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // notepad
        mLlNotepad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, NotepadActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
        // atlas
        mLlAtlas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ins, AtlasActivity.class);
                ActivityCompat.startActivity(ins, intent, options.toBundle());handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, time);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent main = new Intent(this, LoggedActivity.class);
                startActivity(main);
                return true;

            case R.id.logout:
                Toast.makeText(getApplicationContext(), "title", Toast.LENGTH_LONG).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void prepareKnots() {
        int[] covers = new int[]{
                R.drawable.wezel_owijkowy_1,
                R.drawable.zakladanie_zylki_na_szpule,
                R.drawable.wezel_haczylowy_4,
                R.drawable.wezel_palomar_4,
                R.drawable.wezel_barylkowy_4};

        MyKnot a = new MyKnot("Węzeł haczykowy",covers[2],0);
        myKnotList.add(a);

        a = new MyKnot("Węzeł Palomar", covers[3],1);
        myKnotList.add(a);

        a = new MyKnot("Węzeł baryłkowy", covers[4],2);
        myKnotList.add(a);

        a = new MyKnot("Węzeł owijkowy",covers[0],3);
        myKnotList.add(a);

        a = new MyKnot("Zakładanie żyłki na szpulę",covers[1],4);
        myKnotList.add(a);



        adapter.notifyDataSetChanged();
    }

}


