package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import pawel.janiak.com.mobilefishingpersonalassistant.activity.MainActivity;
import pawel.janiak.com.mobilefishingpersonalassistant.R;

public class CompassActivity extends AppCompatActivity implements SensorEventListener {

    // define the display assembly compass picture
    private ImageView mImage;

    // zmienne pozycji
    double targetX, targetY, currentX, currentY;

    // record the compass picture angle turned
    private float currentDegree = 0f;

    // device sensor manager
    private SensorManager mSensorManager;
    TextView tvHeading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);

        mImage = (ImageView) findViewById(R.id.imageViewCompass);

        // TextView that will tell the user what degree is he heading
        tvHeading = (TextView) findViewById(R.id.tvHeading);

        // initialize your android device sensor capabilities
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        // wyciaganie danych z intencji
        Intent mIntent = getIntent();
        LatLng mMyCurrentPosition = mIntent.getParcelableExtra(MainActivity.myCurrentPosition);
        LatLng mTargetPosition = mIntent.getParcelableExtra(MainActivity.targetPosition);

        currentX = mMyCurrentPosition.latitude;
        currentY = mMyCurrentPosition.longitude;
        targetX = mTargetPosition.latitude;
        targetY = mTargetPosition.longitude;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // for the system's orientation sensor registered listeners
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // to stop the listener and save battery
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        // get the angle around the z-axis rotated
        float degree = Math.round(event.values[0]);
        float newAngle = 0;
        double dystans;

        // obliczanie kata
        if (targetX!=currentX) {
            if (targetX > currentX){
                newAngle = (float) Math.toDegrees(Math.atan((targetY - currentY) / (targetX - currentX)));
            }
            if (targetX < currentX){
                newAngle = (float) Math.toDegrees(Math.atan((targetY - currentY) / (targetX - currentX))) + 180;
            }
        }
        degree -=newAngle;

        // obliczanie odleglosci
        dystans = Math.sqrt(Math.abs(Math.pow(targetX - currentX,2))+Math.abs(Math.pow(targetY - currentY,2))) * 111196.672;
        java.text.DecimalFormat df=new java.text.DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);


        tvHeading.setText(
                "Kompas: " + Float.toString(degree)+" stopni\n"
                + "nowy kat: "+newAngle
                + "\ndystans: " +df.format(dystans) +"m"
        );

        // create a rotation animation (reverse turn degree degrees)
        RotateAnimation ra = new RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);

        // how long the animation will take place
        ra.setDuration(210);

        // set the animation after the end of the reservation status
        ra.setFillAfter(true);

        // Start the animation
        mImage.startAnimation(ra);
        currentDegree = -degree;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // not in use
    }
}
