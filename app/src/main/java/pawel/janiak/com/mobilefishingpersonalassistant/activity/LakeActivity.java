package pawel.janiak.com.mobilefishingpersonalassistant.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


import pawel.janiak.com.mobilefishingpersonalassistant.R;

public class LakeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lake_details);

        TextView tvName = (TextView) findViewById(R.id.lake_name);
        TextView tvDesc = (TextView) findViewById(R.id.lake_description);
        TextView tvFish = (TextView) findViewById(R.id.lake_fish);
        TextView tvTips = (TextView) findViewById(R.id.lake_tips);
        TextView tvCoordinates = (TextView) findViewById(R.id.lake_coordinates);

        TextView tvFishTitle = (TextView) findViewById(R.id.lake_fish_title);
        TextView tvTipsTitle = (TextView) findViewById(R.id.lake_tips_title);
        TextView tvCoordinatesTitle = (TextView) findViewById(R.id.lake_coordinates_title);

        Intent mIntent = getIntent();

        tvName.setText(mIntent.getStringExtra(MainActivity.NAZWA_ID));
        tvDesc.setText(mIntent.getStringExtra(MainActivity.OPIS_ID));
        tvFish.setText(mIntent.getStringExtra(MainActivity.RYBY_ID));
        tvTips.setText(mIntent.getStringExtra(MainActivity.PORADY_ID));
        if (mIntent.getDoubleExtra(MainActivity.LAT_ID,0) != 0){
            tvCoordinates.setText("N: "+ mIntent.getDoubleExtra(MainActivity.LAT_ID,0)+" E: "+mIntent.getDoubleExtra(MainActivity.LONG_ID,0));
        } else {
            tvCoordinates.setText(null);
        }


        if (tvDesc.length() == 0){
            tvDesc.setText("brak opisu");
        }
        if (tvFish.length() ==0){
            tvFishTitle.setVisibility(View.INVISIBLE);
        }
        if (tvTips.length() == 0){
            tvTipsTitle.setVisibility(View.INVISIBLE);
        }
        if (tvCoordinates.length() == 0){
            tvCoordinatesTitle.setVisibility(View.INVISIBLE);
        }
    }
}
