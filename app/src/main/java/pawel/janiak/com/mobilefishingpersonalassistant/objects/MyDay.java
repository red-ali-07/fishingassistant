package pawel.janiak.com.mobilefishingpersonalassistant.objects;


public class MyDay {
    private Long id;
    private String data;
    private String wschod;
    private String zachod;
    private String faza;
    private String branie;
    private int thumbnail;

    public MyDay(String data, String wschod, String zachod, String faza, int thumbnail, String branie){
        this.data = data;
        this.wschod = wschod;
        this.zachod = zachod;
        this.faza = faza;
        this.branie = branie;
        this.thumbnail = thumbnail;
    }



    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getData(){
        return data;
    }
    public void setData(String data){
        this.data = data;
    }
    public String getWschod() {
        return wschod;
    }
    public void setWschod(String wschod) {
        this.wschod = wschod;
    }
    public String getZachod() {
        return zachod;
    }
    public void setZachod(String zachod) {
        this.zachod = zachod;
    }
    public String getFaza() {
        return faza;
    }
    public void setFaza(String faza) {
        this.faza = faza;
    }
    public String getBranie(){
        return branie;
    }
    public void setBranie(String branie){
        this.branie = branie;
    }
    public int getThumbnail() {
        return thumbnail;
    }
    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

}