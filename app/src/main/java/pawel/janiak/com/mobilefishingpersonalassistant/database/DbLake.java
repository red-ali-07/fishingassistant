package pawel.janiak.com.mobilefishingpersonalassistant.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import pawel.janiak.com.mobilefishingpersonalassistant.objects.MyLake;

public class DbLake extends SQLiteOpenHelper {

    public DbLake(Context context) {
        super(context, "bazaJezior", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table jeziora(id integer primary key autoincrement, nazwa text, opis text, ryby text, porady text, latitude integer, longitude integer, alpha float);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    public void addMyLake(MyLake myLake){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues wartosci = new ContentValues();
        wartosci.put("nazwa", myLake.getNazwa());
        wartosci.put("opis", myLake.getOpis());
        wartosci.put("ryby", myLake.getRyby());
        wartosci.put("porady", myLake.getPorady());
        wartosci.put("latitude",myLake.getLatitude());
        wartosci.put("longitude",myLake.getLongitude());
        wartosci.put("alpha", myLake.getAlpha());
        db.insertOrThrow("jeziora",null, wartosci);
    }


    public void deleteMyLake(String nazwa){
        SQLiteDatabase db = getWritableDatabase();
        String[] argumenty={""+nazwa};
        db.delete("jeziora", "nazwa=?", argumenty);
    }


    public void updateMyDay(MyLake myLake){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues wartosci = new ContentValues();
        wartosci.put("nazwa", myLake.getNazwa());
        wartosci.put("opis", myLake.getOpis());
        wartosci.put("ryby", myLake.getRyby());
        wartosci.put("porady", myLake.getPorady());
        wartosci.put("latitude",myLake.getLatitude());
        wartosci.put("longitude",myLake.getLongitude());
        wartosci.put("alpha", myLake.getAlpha());
        String args[]={myLake.getId()+""};
        db.update("jeziora", wartosci,"id=?",args);
    }


    public List<MyLake> getAll(){
        List<MyLake> myLakes = new LinkedList<MyLake>();
        String[] kolumny={"id","nazwa","opis","ryby","porady","latitude","longitude","alpha"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor kursor =db.query("jeziora",kolumny,null,null,null,null,null);
        while(kursor.moveToNext()){
            MyLake myLake = new MyLake(null,null,null,null,0,0,0);
            myLake.setId(kursor.getLong(0));
            myLake.setNazwa(kursor.getString(1));
            myLake.setOpis(kursor.getString(2));
            myLake.setRyby(kursor.getString(3));
            myLake.setPorady(kursor.getString(4));
            myLake.setLatitude(kursor.getDouble(5));
            myLake.setLongitude(kursor.getDouble(6));
            myLake.setAlpha(kursor.getFloat(7));
            myLakes.add(myLake);
        }
        return myLakes;
    }
}
